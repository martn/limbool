INSTOPT = --user


install: 
	python setup.py install ${INSTOPT}


.PHONY:  install

# vim: set noexpandtab noautoindent:
