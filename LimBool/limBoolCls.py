#-------------------------- LICENCE BEGIN ---------------------------
# This file is part of LimBool.
#
# LimBool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LimBool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LimBool.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors - Martin Deshaies-Jacques
#
# Copyright 2016 - Air Quality Research Division, 
#                  Environnement And Climate Change Canada
#-------------------------- LICENCE END -----------------------------

class LimBool(int): 
   '''   
   A peculiar integer subclass useful for decreasing verbosity
   in nested function calls.
   LimBool is an unsigned integer with addition and substraction
   where if the result of the operation is negative, it returns 0.
   
   It is used in the module to propagate booleans values in nested
   funtions so that it become *less and less* true.
   Setting 'verbose=LimBool(2)' will stay True for two decrement and
   stay False afterward.

      >>> verbose = LimBool(2)
      >>> for i in xrange(5):
      ...   if verbose-i:
      ...      print('True')
      ...   else:                                             
      ...      print('False')
      True
      True
      False
      False
      False

   Where with a conventional bool or int it would give

      >>> verbose = 2
      >>> for i in xrange(5):
      ...   if verbose: 
      ...      print('True')
      ...   else: 
      ...      print('False')
      ...   verbose -= 1
      True
      True
      False
      True
      True

   since negative numbers are True and only 0 is False.
   '''
   
   def __new__(cls, value=0):
      if value < 0:
         value = 0
      return  super(LimBool, cls).__new__(cls, value)

   def __add__(self, op):
      return self.__class__(super(LimBool, self).__add__(op))

   def __sub__(self, op):
      return self.__class__(super(LimBool, self).__sub__(op))
      
   def __mul__(self, op):
      raise TypeError('LimBool does not admit multiplication') 

   def __div__(self, op):
      raise TypeError('LimBool does not admit division') 
