A peculiar integer subclass useful for decreasing verbosity
in nested function calls.
LimBool is an unsigned integer with addition and substraction
where if the result of the operation is negative, it returns 0.

It is used in the module to propagate booleans values in nested
funtions so that it become *less and less* true.
Setting 'verbose=LimBool(2)' will stay True for two decrement and
stay False afterward.

```python
>>> verbose = LimBool(2)
>>> for i in xrange(5):
...   if verbose-i:
...      print('True')
...   else:                                             
...      print('False')
True
True
False
False
False
```

Where with a conventional bool or int it would give

```python
>>> verbose = 2
>>> for i in xrange(5):
...   if verbose: 
...      print('True')
...   else: 
...      print('False')
...   verbose -= 1
True
True
False
True
True
```

since negative numbers are True and only 0 is False.
