from distutils.core import setup
setup(name='LimBool',
      version='1.0',
      description='Decrementing Boolean',
      author='Martin Deshaies-Jacques',
      author_email='martin.deshaies-jacques@ec.gc.ca',
      packages=[  'LimBool']
      )
